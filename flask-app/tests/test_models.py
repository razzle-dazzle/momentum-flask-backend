import pytest
import datetime

from app.models.models import Exercise, Workout, ExerciseEntry, Set

from tests.utils import TestUtils
from typing import List

RANDOM_DATE_TIME_STR = "2020-04-20 04:20:02.400000"

def test_exercise_class():
  # No name provided
  with pytest.raises(TypeError):
    Exercise()

  new_exercise = Exercise(name= "Test Execise")
  assert new_exercise.name == "Test Execise"
  TestUtils.isUuid(new_exercise.id)
  TestUtils.isDatetime(new_exercise.created_at)

  args = {
    'id' : "123",
    'created_at': RANDOM_DATE_TIME_STR,
    'name': "Test Existing"
  }
  existing_exercise = Exercise(**args)

  assert existing_exercise.name == "Test Existing"
  TestUtils.isUuid(existing_exercise.id)
  assert existing_exercise.id == "123"
  TestUtils.isDatetime(existing_exercise.created_at)
  assert existing_exercise.created_at == RANDOM_DATE_TIME_STR


def test_workout_class():

  new_workout = Workout()
  TestUtils.isUuid(new_workout.id)
  TestUtils.isDatetime(new_workout.created_at)
  assert new_workout.status == "IN_PROGRESS"
  assert new_workout.exercises == []

  ##
  exercise_entry = ExerciseEntry(exercise_id="testid")
  args = {
    'id': "123",
    'created_at': RANDOM_DATE_TIME_STR,
    'exercises': [exercise_entry],
    'status': "COMPLETE"
  }

  existing_workout= Workout(**args)
  TestUtils.isUuid(existing_workout.id)
  TestUtils.isDatetime(existing_workout.created_at)
  assert existing_workout.created_at == RANDOM_DATE_TIME_STR
  assert existing_workout.id == "123"
  assert existing_workout.status == "COMPLETE"
  assert isinstance(existing_workout.exercises[0],ExerciseEntry)

def test_exercise_entry_class():

  ## No exercise Id supplied
  with pytest.raises(TypeError):
    ExerciseEntry()

  args= {
    'exercise_id': "123",
  }
  new_exercise_entry = ExerciseEntry(**args)
  TestUtils.isUuid(new_exercise_entry.id)
  TestUtils.isUuid(new_exercise_entry.exercise_id)
  assert new_exercise_entry.exercise_id == "123"
  TestUtils.isDatetime(new_exercise_entry.created_at)
  assert new_exercise_entry.sets == []
  
  set_ = Set({'weight': 7.5, 'reps': 1})
  args.update({
    'created_at': RANDOM_DATE_TIME_STR,
    'sets': [set_]
  })

  existing_exercise_entry = ExerciseEntry(**args)
  TestUtils.isUuid(existing_exercise_entry.id)
  TestUtils.isUuid(existing_exercise_entry.exercise_id)
  assert existing_exercise_entry.exercise_id == "123"
  TestUtils.isDatetime(existing_exercise_entry.created_at)
  assert existing_exercise_entry.created_at == RANDOM_DATE_TIME_STR
  assert isinstance(existing_exercise_entry.sets[0], Set)


def test_set_class():

  new_set = Set()
  assert new_set.weight == 0.0
  assert new_set.reps == 0

  existing_set = Set(**{'weight': 7.5, 'reps': 20})
  assert existing_set.weight == 7.5
  assert existing_set.reps == 20