import pytest
import datetime
from flask import g, session
from app.db import get_db

from app.models.models import Exercise
from tests.utils import TestUtils

def test_get_exercises(client,app):
  with app.app_context():
    with client:
      response= client.get('/exercises/')
      assert response.status_code == 200 , "got exercise"
      assert g.user['id'] == '1' , "user has been set"

def test_get_exercise(client,app):
  
  with app.app_context():
    with client:
      response = client.get('/exercises/1')
      assert response.status_code == 200 , "got exercise"
      data = response.get_json()
      TestUtils.isUuid(data['id'])
      TestUtils.isDatetime(data['created_at'])
      assert type(data['name']) == str and len(data['name']) > 0

      assert g.user['id'] == '1' , "user has been set"

def test_get_fake_exercise(client,app):
  with app.app_context():
    with client:
      response = client.get('/exercises/fake-exercise')
      assert response.status_code == 404 , "should be 404"

def test_create_exercise_no_body(client,app):
  with app.app_context():
    with client:
      response = client.post('/exercises/')
      assert response.status_code == 400 , "should be 400"

def test_create_exercise(client,app):
  with app.app_context():
    with client:
      now = datetime.datetime.utcnow()
      response = client.post('/exercises/', json={
          "name": f'Test exercise{now}',
      })
      json_data = response.get_json()

      assert response.status_code == 200 , "should be 200"
      TestUtils.isUuid(json_data['id'])
      TestUtils.isDatetime(json_data['created_at'])
      assert type(json_data['name']) == str and len(json_data['name']) > 0

