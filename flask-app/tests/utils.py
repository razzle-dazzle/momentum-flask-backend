import datetime
import pytest
class TestUtils:
  @staticmethod
  def isDatetime(str_datetime):
    date_format = "%Y-%m-%d %H:%M:%S.%f"
    date_to_assert = datetime.datetime.strptime(str_datetime,date_format)
    assert isinstance(date_to_assert, datetime.datetime)
  
  @staticmethod
  def isUuid(id):
    assert type(id) is str and len(id) > 0