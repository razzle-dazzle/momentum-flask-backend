import pytest
import datetime
from flask import g, session
from app.db import get_db

from app.models.models import Workout
from tests.utils import TestUtils

def test_get_workouts(client,app):
  with app.app_context():
    with client:
      response= client.get('/workouts/')
      assert response.status_code == 200 , "got workouts"
      assert g.user['id'] == '1' , "user has been set"

def test_get_workout(client,app):
  
  with app.app_context():
    with client:
      response = client.get('/workouts/1')
      assert response.status_code == 200 , "got workout"
      data = response.get_json()
      TestUtils.isUuid(data['id'])
      TestUtils.isDatetime(data['created_at'])

def test_get_fake_workout(client,app):
  with app.app_context():
    with client:
      response = client.get('/workouts/fake-workout')
      assert response.status_code == 404 , "should be 404"

def test_create_workout(client,app):
  with app.app_context():
    with client:
      response = client.post('/workouts/')
      json_data = response.get_json()

      assert response.status_code == 200 , "should be 200"
      TestUtils.isUuid(json_data['id'])
      TestUtils.isDatetime(json_data['created_at'])
      assert json_data['status'] == "IN_PROGRESS"


def test_finish_workout(client,app):
  with app.app_context():
    with client:
      ##Create workout
      client.post('/workouts/')

      response = client.post('/workouts/current/finish')

      assert response.status_code == 204 , "should be 204"


def test_delete_workout(client,app):
  with app.app_context():
    with client:
      ##Create workout
      client.post('/workouts/')

      response = client.delete('/workouts/current')

      assert response.status_code == 204 , "should be 204"