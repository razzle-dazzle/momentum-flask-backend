import os
import tempfile

import pytest
from app import create_app
from app.db import get_db, init_db

@pytest.fixture
def app():
  
  os.environ.update({'FLASK_ENV': 'testing'})
  app = create_app()

  with app.app_context():
    init_db()

  yield app

@pytest.fixture
def client(app):
    return app.test_client()
