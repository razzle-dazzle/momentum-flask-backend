import pytest
import datetime
from flask import g, session
from app.db import get_db

from app.models.models import ExerciseEntry
from tests.utils import TestUtils

def test_get_exercise_entries(client,app):
  with app.app_context():
    with client:
      response= client.get('/workouts/<id>/exercises/')
      json_data = response.get_json()

      assert response.status_code == 200 , "got exercise"
      assert isinstance(json_data,list)

def test_create_exercise_entry_no_body(client,app):
  with app.app_context():
    with client:
      response = client.post('/workouts/1/exercises/')
      assert response.status_code == 400 , "should be 400"

def test_create_exercise_entry(client,app):
  with app.app_context():
    with client:
      response = client.post('/workouts/1/exercises/', json={
        "workout_id": "1",
        "exercise_id": '1',
        "sets": [{
          'weight': 20,
          'reps': 10
        }]
      })

      json_data = response.get_json()
      
      assert response.status_code == 200 , "should be 200"
      TestUtils.isUuid(json_data['id'])
      TestUtils.isDatetime(json_data['created_at'])
      assert isinstance(json_data['sets'], list) , "sets are not a list"

def test_create_exercise_entry_no_workout_id(client,app):
  with app.app_context():
    with client:
      response = client.post('/workouts/1/exercises/', json={
        "exercise_id": '1',
        "sets": [{
          'weight': 20,
          'reps': 10
        }]
      })

      assert response.status_code == 400 , "should be 400"


def test_update_exercise_entry(client,app):
  with app.app_context():
    with client:
      response = client.patch('/workouts/1/exercises/ex_entry-1', json={
        "sets": [{
          'weight': 20,
          'reps': 10
        },
        {
          'weight': 40,
          'reps': 50
        }]
      })

      assert response.status_code == 204 , "should be 204"

def test_update_fake_exercise_entry(client,app):
  with app.app_context():
    with client:
      response = client.patch('/workouts/1/exercises/fake_ex_entry', json={
        "sets": [{
          'weight': 20,
          'reps': 10
        },
        {
          'weight': 40,
          'reps': 50
        }]
      })

      assert response.status_code == 404 , "should be 404"

## TODO Do not rely on the create endpoint
def test_delete_exercise_entry(client,app):
  with app.app_context():
    with client:
      response = client.post('/workouts/1/exercises/', json={
        "workout_id": "1",
        "exercise_id": '1',
        "sets": [{
          'weight': 20,
          'reps': 10
        }]
      })

      json_data = response.get_json()
      exercise_entry_id= json_data['id']

      response = client.delete(f'/workouts/1/exercises/{exercise_entry_id}')
      
      assert response.status_code == 204 , "should be 204"