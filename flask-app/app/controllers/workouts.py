from flask import request, g

import app.db as db
from app.models.models import Exercise, Workout

def get_workouts():
  user_id = g.user['id']

  database = db.get_db()

  workouts = database.get_workouts(user_id)

  return workouts

def create_workout():
  user_id = g.user['id']
  
  workout = Workout()

  database = db.get_db()

  current_workout = database.get_workout(user_id, "IN_PROGRESS")

  if current_workout is None:
    database.create_workout(user_id, workout)
    return workout
  else:
    return current_workout

def get_workout(id):

  user_id = g.user['id']

  database = db.get_db()

  workout = database.get_workout(user_id= user_id, workout_id = id)

  return workout

def delete_workout(id) -> bool:
  user_id = g.user['id']

  database = db.get_db()

  deleted =  database.delete_workout(user_id= user_id, workout_id = id)

  return deleted

def finish_current_workout() -> bool:
  user_id = g.user['id']

  database = db.get_db()

  updated =  database.finish_current_workout(user_id= user_id)

  return updated