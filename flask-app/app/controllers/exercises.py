from flask import request, g

import app.db as db
from app.models.models import Exercise, ExerciseEntry, Set

def get_exercises():
  user_id = g.user['id']

  database = db.get_db()

  exercises = database.get_exercises(user_id)

  return exercises

def create_exercise():
  user_id = g.user['id']
  
  body = request.json

  args = {
    'name': body['name']
  }
  
  exercise = Exercise(**args)

  database = db.get_db()

  exercise = database.create_exercise(user_id,exercise)

  return exercise

def get_exercise(id):

  user_id = g.user['id']

  database = db.get_db()

  exercise = database.get_exercise(user_id= user_id, exercise_id = id)

  return exercise

#region Exercise Entries  

def get_exercise_entries(workout_id):

  database = db.get_db()

  exercise_entries = database.get_exercise_entries(workout_id = workout_id)

  return exercise_entries

def create_exercise_entry(workout_id):

  database = db.get_db()

  body = request.json
  ## TODO Find better logic
  if workout_id == "current":
    workout_id = body['workout_id']

  sets_ = []
  if 'sets' in body and body['sets']:
    for set_ in body['sets']:
      sets_.append(Set(**set_))

  body['sets'] = sets_
  
  exercise_entry = ExerciseEntry(**body)

  database.create_exercise_entry(workout_id = workout_id, exercise_entry= exercise_entry)

  return exercise_entry

def delete_exercise_entry(workout_id,exercise_entry_id) -> bool:

  database = db.get_db()

  deleted = database.delete_exercise_entry(workout_id = workout_id,exercise_entry_id=exercise_entry_id)

  return deleted

def update_exercise_entry(workout_id, exercise_entry_id):

  body = request.json
  sets = []
  if 'sets' in body and body['sets']:
    for set_ in body['sets']:
      sets.append(Set(**set_))

  database = db.get_db()

  updated = database.update_exercise_entry(workout_id, exercise_entry_id, sets)

  return updated
#endregion