from flask import (
    Blueprint, flash, g, redirect, request, url_for, Response, jsonify
)
from werkzeug.exceptions import abort

# from app.auth import login_required
from app.db import get_db
from app.controllers import exercises as exercises_controller
from app.routes.http.validations import validate_request
from app.middleware.auth import login_required

bp = Blueprint('exercises', __name__, url_prefix='/exercises')

# def load_logged_in_user():
#   g.user = {'id' : '1'}

@bp.route('/', methods=('GET', 'POST'))
@login_required
def exercises():
  if request.method == 'GET':
    
    exercises = exercises_controller.get_exercises()

    if exercises is not None:
      return jsonify(exercises)

    return Response(status=404)

  if request.method == 'POST':
    @validate_request(schema="create_exercise")
    def post_exercises():
      return exercises_controller.create_exercise()

    exercise = post_exercises()

    if exercise is None:
      return Response(status=400)

    return jsonify(exercise)

@bp.route('/<id>', methods=('GET', 'POST','PUT','PATCH','DELETE'))
@login_required
def exercise_definition(id):
  if request.method == 'GET':

    exercise = exercises_controller.get_exercise(id)

    if exercise is None:
      return Response(status=404)
    
    return jsonify(exercise)
    
  # if request.method == 'PATCH':
  #   return exercises_controller.create_exercise()