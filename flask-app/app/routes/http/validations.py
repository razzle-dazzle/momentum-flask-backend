import json
import jsonschema
from jsonschema import validate
from flask import request, abort
import functools

create_exercise = {
    "type": "object",
    "properties": {
        "name": {"type": "string"},
        "id": {"type": "string"},
    },
    "required": ["name"]
  }

set_entry = {
    "type": "object",
    "properties": {
        "weight": {"type": "number"},
        "reps": {"type": "number"},
    }
  } 

create_exercise_entry = {
    "type": "object",
    "properties": {
        "workout_id": {"type": "string"},
        "exercise_id": {"type": "string"},
        "id": {"type": "string"},
        "sets": {
          "type": "array" ,
          "items": set_entry
        }
    },
    "required": ["workout_id","exercise_id"]
  }



def validateJson(jsonData,schema):
    try:
        validate(instance=jsonData, schema=schema)
    except jsonschema.exceptions.ValidationError as err:
        return False
    return True


schemas = {
  'create_exercise' : create_exercise,
  'create_exercise_entry': create_exercise_entry
}


def validate_request(schema):
  def decorator(route_handler):
    @functools.wraps(route_handler)
    def validation_wrapper(**kwargs):
      body = request.json
      valid = validateJson(body,schemas[schema])
      if valid is True:
        return route_handler(**kwargs)
      else:
        abort(400)

    return validation_wrapper
  return decorator