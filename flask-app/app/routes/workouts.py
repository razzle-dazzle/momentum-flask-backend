from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, jsonify, Response
)
from werkzeug.exceptions import abort

# from app.auth import login_required
from app.db import get_db
from app.controllers import workouts as workouts_controller
from app.controllers import exercises as exercises_controller
from app.routes.http.validations import validate_request
from app.middleware.auth import login_required

bp = Blueprint('workouts', __name__, url_prefix='/workouts')

# @bp.before_app_request
# def load_logged_in_user():
#   g.user = {'id' : '1'}

@bp.route('/', methods=('GET', 'POST'))
@login_required
def workouts():
  if request.method == 'GET':
    return jsonify(workouts_controller.get_workouts())
    
  if request.method == 'POST':
    return jsonify(workouts_controller.create_workout())

@bp.route('/current/finish', methods=('GET', 'POST'))
@login_required
def finish_current_workout():
  if request.method == 'POST':
    #Finish current workout
    updated = workouts_controller.finish_current_workout()
    if updated is False:
      return Response(status=404)
    return Response(status=204)

@bp.route('/<id>', methods=('GET', 'POST','PUT','PATCH','DELETE'))
@login_required
def workout(id):
  if request.method == 'GET':
    workout = workouts_controller.get_workout(id)

    if workout is None:
      return Response(status=404)

    return jsonify(workout)

  if request.method == 'DELETE':
    deleted = workouts_controller.delete_workout(id)

    if not deleted:
      return Response(status=400)

    return Response(status=204)


## TODO Figure out how to mix blueprints
@bp.route('/<workout_id>/exercises/', methods=('GET', 'POST','PATCH','DELETE'), strict_slashes=False)
@login_required
def exercise_entries(workout_id):
  if request.method == 'GET':
    exercise_entries = exercises_controller.get_exercise_entries(workout_id=workout_id)

    if exercise_entries is None:
      return Response(status=404)

    return jsonify(exercise_entries)

  if request.method == 'POST':
    @validate_request(schema="create_exercise_entry")
    def create_exercise_entry():
      return exercises_controller.create_exercise_entry(workout_id=workout_id)

    exercise_entry = create_exercise_entry()

    if exercise_entry is None:
      return Response(status=404)

    return jsonify(exercise_entry)

@bp.route('/<workout_id>/exercises/<exercise_entry_id>', methods=('PATCH','DELETE'))
@login_required
def exercise_entry(workout_id, exercise_entry_id):
  if request.method == 'DELETE':
    deleted = exercises_controller.delete_exercise_entry(
      workout_id=workout_id, 
      exercise_entry_id=exercise_entry_id
      )

    if not deleted:
      return Response(status=404)

    return Response(status=204)

  if request.method == 'PATCH':
    updated = exercises_controller.update_exercise_entry(
      workout_id=workout_id, 
      exercise_entry_id=exercise_entry_id
      )

    if not updated:
      return Response(status=404)

    return Response(status=204)