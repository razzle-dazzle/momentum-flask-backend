import os
import functools
from flask import request, Response, g
import jwt
# from jwt.contrib.algorithms.pycrypto import RSAAlgorithm
def decode_auth_token(auth_token):
  """
  Decodes the auth token
  :param auth_token:
  :return: integer|string
  """
  try:
    payload = jwt.decode(auth_token, key=os.environ.get('JWT_PUBLIC_KEY'), algorithms=["RS256"]) #
    return payload
  except jwt.ExpiredSignatureError:
    return 'Signature expired. Please log in again.'
  except jwt.InvalidTokenError:
    return 'Invalid token. Please log in again.'

def login_required(api_call):
  @functools.wraps(api_call)
  def wrapped_api_call(**kwargs):

    token = request.cookies.get('token')

    if not token:
      return Response(status=401)

    decoded = decode_auth_token(token)

    if isinstance(decoded,str):
      return Response(response=decoded, status=401)
    else:
      g.user= decoded

    return api_call(**kwargs)

  return wrapped_api_call
