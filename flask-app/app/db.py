from flask import g, current_app

from app.database.db_dynamodb import DynamoDB

def init_db():
  if 'db' not in g:
    ## DB interface returned here
    _options={
      'aws_access_key_id':     current_app.config['AWS_ACCESS_KEY_ID'], 
      'aws_secret_access_key': current_app.config['AWS_SECRET_ACCESS_KEY'],
      'region_name':           current_app.config['AWS_DYNAMODB_REGION'],
      'endpoint_url':          current_app.config['AWS_DYNAMODB_ENDPOINT']
    }
    g.db = DynamoDB(**_options)
    return g.db

def get_db():
    if 'db' not in g:
        init_db()

    return g.db

def close_db(e=None):
    db = g.pop('db', None)

    if db is not None:
        db.close()