from __future__ import print_function
import boto3
from boto3.dynamodb.conditions import Key, Attr

import json
import sys
import decimal
import datetime
from flask import jsonify
from werkzeug.exceptions import abort

from app.models.models import Workout, Exercise, ExerciseEntry, Set
from app.database.db_interface import DatabaseInterface

class DynamoDB(DatabaseInterface):
  _options={
    'service_name':           'dynamodb',
    'aws_access_key_id':      None, 
    'aws_secret_access_key':  None,
    'endpoint_url':           None
  }

  def __init__(self,**kwargs):
    # Override options
    self._options.update(kwargs)
    
    self.client = boto3.resource(**self._options)

  def _dynamo_response_to_json(self,data):
    if data is None:
      return None
    return json.loads(json.dumps(data, cls=DecimalEncoder))

  def _item_to_dynamo_item(self,data):
    if data is None:
      return None
    return json.loads(json.dumps(data), parse_float=decimal.Decimal)
  
  #region Workouts

  def get_workout(self, user_id: str, workout_id: str, **kwargs ):
    """
    USER AND WORKOUT IDs needed
    """
    if workout_id.lower() == "current":
      workout_id= "IN_PROGRESS"

    PartitionKey= f'USER#{user_id}'
    SortKey = f'WORKOUT#{workout_id}'

    Key = {
      'pk' : PartitionKey,
      'sk' : SortKey
    }

    response = self.client.Table("momentum").get_item(
        Key= Key
    )

    if 'Item' in response:
      if 'raw' in kwargs and kwargs['raw'] is True:
        return response['Item']

      json_data = self._dynamo_response_to_json(response['Item'])
      workout = workout_from_item(json_data)
      return workout
    else:
      return None

  def get_workouts(self, user_id: str):
    PartitionKey= f'USER#{user_id}'
    SortKey = f'WORKOUT#'

    response = self.client.Table("momentum").query(
        KeyConditionExpression= Key('pk').eq(PartitionKey) & Key('sk').begins_with(SortKey)
    )
    workouts = []

    if 'Items' in response and response['Items']:
      json_data = self._dynamo_response_to_json(response['Items'])
      for item in json_data:
        workouts.append(workout_from_item(item))

    return workouts

  def create_workout(self, user_id: str, workout: Workout):
    PartitionKey= f'USER#{user_id}'
    SortKey = f'WORKOUT#IN_PROGRESS'
    WorkoutStatus = workout.status

    Item = {
      'pk': PartitionKey,
      'sk': SortKey,
      'id': workout.id,
      'created_at': workout.created_at,
      'status': WorkoutStatus,
      'WorkoutStatusDate': WorkoutStatus
    }

    self.client.Table("momentum").put_item(
        Item = Item
    )
     
    return workout

  def finish_current_workout(self, user_id: str):
    current_workout = self.get_workout(user_id, "IN_PROGRESS",raw=True)
    
    if current_workout is None:
      return False

    current_workout['sk'] = f'WORKOUT#{current_workout["id"]}'
    current_workout['status'] = "COMPLETED"
    current_workout['WorkoutStatusDate'] = f'COMPLETED|{str(datetime.datetime.utcnow().isoformat())}'


    ## TODO change to transact_write
    self.client.Table("momentum").put_item(
        Item = current_workout
    )

    deleted = self.delete_workout(user_id,"IN_PROGRESS")
    
    return deleted

  def delete_workout(self, user_id: str, workout_id):
    if workout_id.lower() == "current":
      workout_id= "IN_PROGRESS"

    Key = {
      'pk': f'USER#{user_id}',
      'sk': f'WORKOUT#{workout_id}'
    }

    response = self.client.Table("momentum").delete_item(
        Key = Key,
        ReturnValues="ALL_OLD"
    )

    ## Fire off events to delete the exercise entries associated with it
    if 'Attributes' in response:
      return True
    else:
      return False

  #endregion

  #region Exercises

  def get_exercises(self, user_id: str):
    PartitionKey= f'USER#{user_id}'
    SortKey = f'EXERCISE'

    response = self.client.Table("momentum").query(
      KeyConditionExpression= Key('pk').eq(PartitionKey) & Key('sk').begins_with(SortKey)
    )

    exercises= []

    if 'Items' in response and response['Items']:
      json_data = self._dynamo_response_to_json(response['Items'])
      exercises= exercises_from_items(json_data)

    return exercises

  def get_exercise(self, user_id: str, exercise_id: str):

    PartitionKey= f'USER#{user_id}'
    SortKey = f'EXERCISE#{exercise_id}'

    response = self.client.Table("momentum").get_item(
        Key= {
          'pk': PartitionKey,
          'sk': SortKey
        }
    )
    
    if 'Item' not in response:
      return None

    json_data = self._dynamo_response_to_json(response['Item'])
    exercise = exercise_from_item(json_data)
    return exercise

  def create_exercise(self, user_id, exercise: Exercise):

    PartitionKey= f'USER#{user_id}'
    SortKey = f'EXERCISE#{exercise.id}'
    Item = {
      'pk': PartitionKey,
      'sk': SortKey,
      'id': exercise.id,
      'name': exercise.name,
      'created_at': exercise.created_at
    }

    response = self.client.Table("momentum").put_item(
        Item = Item
    )
    
    if response['ResponseMetadata']['HTTPStatusCode'] != 200:
      return None

    return exercise

  #endregion

  #region Exercise Entries
  def get_exercise_entries(self, workout_id: str) -> list:
    PartitionKey= f'WORKOUT#{workout_id}'
    SortKey= f'EX_WORKOUT#'

    response = self.client.Table("momentum").query(
      IndexName="GSI_sk-pk",
      KeyConditionExpression= Key('sk').eq(PartitionKey) & Key('pk').begins_with(SortKey)
    )
    if 'Items' not in response:
      return []

    json_data = self._dynamo_response_to_json(response['Items'])

    exercise_entries = exercise_entries_from_items(json_data)

    return exercise_entries

  def get_exercise_entry(self, workout_id: str, exercise_entry_id: str):
    PartitionKey= f'EX_WORKOUT#{exercise_entry_id}'
    SortKey= f'WORKOUT#{workout_id}'

    key = {
      'pk': PartitionKey,
      'sk': SortKey
    }

    response = self.client.Table("momentum").get_item(
      Key= key
    )
    if 'Item' not in response:
      return None

    json_data = self._dynamo_response_to_json(response['Item'])
    exercise_entry = exercise_entry_from_item(json_data)

    return exercise_entry

  def create_exercise_entry(self, workout_id: str, exercise_entry: ExerciseEntry) -> bool:
    PartitionKey= f'EX_WORKOUT#{exercise_entry.id}'
    SortKey = f'WORKOUT#{workout_id}'

    Item = {
      'pk': PartitionKey,
      'sk': SortKey,
      'id': exercise_entry.id,
      'sets': items_from_sets(exercise_entry.sets),
      'exercise_id': exercise_entry.exercise_id,
      'created_at': exercise_entry.created_at
    }

    Item = self._item_to_dynamo_item(Item)

    self.client.Table("momentum").put_item(
        Item = Item
    )

    return True

  def delete_exercise_entry(self, workout_id,exercise_entry_id):
    PartitionKey= f'EX_WORKOUT#{exercise_entry_id}'
    SortKey = f'WORKOUT#{workout_id}'

    Key = {
      'pk': PartitionKey,
      'sk': SortKey
    }

    response = self.client.Table("momentum").delete_item(
        Key = Key,
        ReturnValues="ALL_OLD"
    )

    ## Fire off events to delete the exercise entries associated with it
    if 'Attributes' in response:
      return True
    else:
      return False

  def update_exercise_entry(self, workout_id, exercise_entry_id, sets):
    PartitionKey= f'EX_WORKOUT#{exercise_entry_id}'
    SortKey = f'WORKOUT#{workout_id}'

    entry= self.get_exercise_entry(
      workout_id=workout_id, 
      exercise_entry_id=exercise_entry_id
    )

    if entry is None:
      return False

    Key = {
      'pk': PartitionKey,
      'sk': SortKey
    }

    ## TODO Clean this up
    updatedSets = []
    AttributeSets= items_from_sets(sets)

    for attrSet in AttributeSets:
      updatedSets.append(self._item_to_dynamo_item(attrSet))

    response = self.client.Table("momentum").update_item(
        Key = Key,
        UpdateExpression = "set #sets = :s",
        ExpressionAttributeValues={
        ':s': updatedSets},
        ExpressionAttributeNames={
        '#sets': 'sets'},
        ReturnValues="UPDATED_NEW"
    )

    ## Fire off events to delete the exercise entries associated with it
    if 'Attributes' in response:
      return True
    else:
      return False

  #endregion

class DecimalEncoder(json.JSONEncoder):
  def default(self, o):
      if isinstance(o, decimal.Decimal):
          if o % 1 > 0:
              return float(o)
          else:
              return int(o)
      return super(DecimalEncoder, self).default(o)


##TODO rewrite
## All related to dynamodb -> move there
def workout_from_item(item,**kwargs):
  if 'exercises' in kwargs and kwargs['exercises']:
    item['exercises']= kwargs['exercises']

  workout = Workout(**item)
  return workout

def exercise_entries_from_items(items):
  exercises= []
  for exercise_entry_item in items:
    exercises.append(exercise_entry_from_item(exercise_entry_item))
  return exercises

def exercises_from_items(items):
  exercises= []
  
  for exercise_item in items:
    exercises.append(exercise_from_item(exercise_item))
    
  return exercises

def exercise_from_item(item):
  extras = {
    'created_at' : item['created_at'],
    'id'         : item['id']
    }
  return Exercise(item['name'],**extras)

def exercise_entry_from_item(item):
  if 'sets' in item and item['sets']:
    _sets = sets_from_items(item['sets'])
    item['sets'] = _sets
  
  exercise_entry = ExerciseEntry(**item)
  return exercise_entry

def sets_from_items(items):
  sets = []
  for set_ in items:
    sets.append(set_from_item(set_))
  return sets

def set_from_item(item):
  set_=Set(**item)
  return set_

def items_from_sets(sets):
  resolved_sets= []

  for set_ in sets:
    resolved_sets.append(set_.__dict__)

  return resolved_sets