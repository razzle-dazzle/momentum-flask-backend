from decimal import Decimal
import datetime
batch_items = {'Items': [
  {'pk': 'EX_WORKOUT#ex_entry-1', 'sk': 'WORKOUT#1', 'sets': [{'weight': Decimal('0'), 'reps': Decimal('0')}], 'exercise_id': '2', 'created_at': '2020-04-14 14:23:23.753163', 'id': 'ex_entry-1'}, 
  {'pk': 'EX_WORKOUT#ex_entry-2', 'sk': 'WORKOUT#1', 'sets': [{'weight': Decimal('40'), 'reps': Decimal('30')}], 'exercise_id': '1', 'created_at': '2020-04-10 10:55:20.000420', 'id': 'ex_entry-2', 'type': 'EXERCISE#1'}, 
  {'pk': 'EX_WORKOUT#ex_entry-3', 'sk': 'WORKOUT#1', 'sets': [{'weight': Decimal('0'), 'reps': Decimal('0')}], 'exercise_id': '2', 'created_at': '2020-04-14 14:24:34.379679', 'id': 'ex_entry-3'}, 
  {'pk': 'EX_WORKOUT#ex_entry-4', 'sk': 'WORKOUT#1', 'sets': [{'weight': Decimal('50'), 'reps': Decimal('20')}], 'exercise_id': '2', 'created_at': '2020-04-14 14:41:25.136533', 'id': 'ex_entry-4'}, 
  {'pk': 'USER#1', 'sk': 'EXERCISE#1', 'id' : "1", 'created_at': '2020-04-10 15:04:20.000420', 'name': 'Pushups'}, 
  {'pk': 'USER#1', 'sk': 'EXERCISE#2', 'id' : "2",'created_at': '2020-04-10 16:04:20.000420', 'name': 'Pullups'}, 
  {'pk': 'USER#1', 'sk': 'EXERCISE#ex-3', 'id' : "ex-3",'created_at': '2020-04-11 19:03:50.712353', 'name': 'Situps'}, 
  {'pk': 'USER#1', 'sk': 'PROFILE#1', 'password': 'password', 'username': 'sven', 'created_at': '2020-04-10 14:45:20.000420', 'email': 'sven@sven.com'}, 
  {'pk': 'USER#1', 'sk': 'WORKOUT#0', 'created_at': '2020-04-09 10:49:20.000420', 'WorkoutStatusDate': 'COMPLETED|2020-04-10 10:59:20.000420', 'id': '0', 'status': 'COMPLETED'}, 
  {'pk': 'USER#1', 'sk': 'WORKOUT#1', 'created_at': '2020-04-10 10:49:20.000420', 'WorkoutStatusDate': 'COMPLETED|2020-04-10 11:05:20.000420', 'id': '1', 'status': 'COMPLETED'},
  {'pk': 'USER#1', 'sk': 'WORKOUT#wk-2', 'created_at': '2020-04-15 13:12:10.328036', 'id': 'wk-2', 'WorkoutStatusDate': 'COMPLETED|2020-04-15 13:23:49.816544', 'status': 'COMPLETED'}
  ]
}