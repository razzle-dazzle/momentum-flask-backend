import os
class Config(object):
  DEBUG = False
  TESTING = False
  FLASK_ENV=os.environ.get('FLASK_ENV') or 'production'

  
  AWS_REGION_NAME=os.environ.get('AWS_REGION_NAME') or "eu-west-2"
  AWS_SECRET_ACCESS_KEY=None
  AWS_ACCESS_KEY_ID=None
  AWS_DYNAMODB_ENDPOINT=None
  AWS_DYNAMODB_REGION="eu-west-2"
  JWT_PUBLIC_KEY=os.environ.get('JWT_PUBLIC_KEY') or 'secret'
  API_ALLOWED_ORIGINS= []
  
  # Do that
  DYNAMODB_CONFIG={'AWS_REGION': AWS_REGION_NAME}


class productionConfig(Config):
  AWS_ACCESS_KEY_ID=os.environ.get('AWS_ACCESS_KEY_ID')
  AWS_SECRET_ACCESS_KEY=os.environ.get('AWS_SECRET_ACCESS_KEY')
  API_ALLOWED_ORIGINS= ['https://momentvm.co.uk']
  
class developmentConfig(Config):
  DEBUG = True
  FLASK_ENV="development"
  AWS_DYNAMODB_REGION="localhost"
  AWS_DYNAMODB_ENDPOINT = 'http://0.0.0.0:3369'
  API_ALLOWED_ORIGINS= ['http://localhost.com*']

class testingConfig(Config):
  TESTING = True
  FLASK_ENV="testing"
  Config.FLASK_ENV
  AWS_DYNAMODB_ENDPOINT = 'http://0.0.0.0:3369'
  AWS_DYNAMODB_REGION="localhost"
  API_ALLOWED_ORIGINS= ['http://localhost.com*']

