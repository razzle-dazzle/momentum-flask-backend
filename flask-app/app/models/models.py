from __future__ import annotations
import uuid
import datetime

import os
from dataclasses import dataclass, field
from typing import List

import boto3
from boto3.dynamodb.conditions import Key

### https://docs.python.org/3/library/dataclasses.html

def get_from_kwargs(kwargs,key,default_value):
  resolved_value= None
  if key in kwargs and kwargs[key]:
    resolved_value = kwargs[key]
  else:
    resolved_value = default_value
  return resolved_value

def date_now():
  return datetime.datetime.utcnow().isoformat()

@dataclass
class Workout:
  id: str
  created_at: str
  status: str
  exercises: List[ExerciseEntry]

  def __init__(self,*args,**kwargs):

    self.id =           get_from_kwargs(kwargs,'id'         ,str(uuid.uuid4()))
    self.status =       get_from_kwargs(kwargs,'status'     ,"IN_PROGRESS")
    self.exercises =    get_from_kwargs(kwargs,'exercises'  ,[])
    self.created_at =   get_from_kwargs(kwargs,'created_at' ,str(date_now()))

  def set_status(self,status):
    self.status= status


@dataclass
class ExerciseEntry:
  id: str
  exercise_id: str
  created_at: str
  sets: List[Set]
  def __init__(self,exercise_id,*args,**kwargs):
    ## TODO: throw error when exercise_id not specified
    self.exercise_id =  exercise_id
    self.id =           get_from_kwargs(kwargs,'id'         ,str(uuid.uuid4()))
    self.created_at =   get_from_kwargs(kwargs,'created_at' ,str(date_now()))
    self.sets =         get_from_kwargs(kwargs,'sets'       ,[])
@dataclass
class Set:
  weight: float
  reps: int
  def __init__(self,*args,**kwargs):
    self.weight =       get_from_kwargs(kwargs,'weight'    ,0.0)
    self.reps   =       get_from_kwargs(kwargs,'reps'      ,0)

@dataclass
class Exercise:
  id: str
  created_at: str
  # _type: str
  name: str

  def __init__(self,name,**kwargs):
    self.id =           get_from_kwargs(kwargs,'id'         ,str(uuid.uuid4()))
    self.name =         name
    # self._type =        _type
    self.created_at =   get_from_kwargs(kwargs,'created_at' ,str(date_now()))