from werkzeug.utils import import_string
from flask import Flask, render_template, g, request, Response
from flask_cors import CORS, cross_origin
import os
import sys
from . import db


def create_app():
  app =  Flask(__name__, instance_relative_config=True)
  print("Python - Version: ", sys.version)

  #config.py
  ENV = os.environ.get('FLASK_ENV') or 'production'
  app.config.from_object(f'app.config.{ENV}Config')

  CORS(app, supports_credentials=True, origins=app.config['API_ALLOWED_ORIGINS'])
  
  @app.route('/healthcheck', methods=['GET'])
  def healthcheck():
    if request.method == 'GET':
      return Response(status=200)


  from app.routes import exercises
  app.register_blueprint(exercises.bp)

  from app.routes import workouts
  app.register_blueprint(workouts.bp)

  return app
