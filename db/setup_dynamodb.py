# import click
import boto3
import table_definition

options = {
  'service_name' : 'dynamodb',
  'endpoint_url' : 'http://localhost:3369',
  'region_name':   "localhost"
}
client = boto3.client(**options)
resource = boto3.resource(**options)

def list_tables():
  print("Printing tables:\n###############")
  tables = client.list_tables()
  print(tables['TableNames'])
  print("###############")

def delete_table(table_name):
  print("Deleting table...")
  client.delete_table(TableName=table_name)
  print("Table deleted")

def create_table(table_name):
  tables = client.list_tables()['TableNames']
  if table_name not in tables:
    print('Table does not exist. Creating table...')
    definition = table_definition.get_definition()
    client.create_table(**definition)
    print('Table created')
  list_tables()

def plant_seeds(table_name):
  from seed_dynamodb import batch_items
  table = resource.Table(table_name)

  with table.batch_writer() as batch:
    for item in batch_items['Items']:
      batch.put_item(item)

def purge_seeds(table_name):
  delete_table(table_name)
  create_table(table_name)

list_tables()
