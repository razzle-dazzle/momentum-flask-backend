def get_definition():
    return {
      'TableName':'momentum',
      'AttributeDefinitions': [
        {
              'AttributeName': 'pk',
              'AttributeType': 'S'
        },
        {
              'AttributeName': 'sk',
              'AttributeType': 'S'
        },
        {
                'AttributeName': 'WorkoutStatusDate',
                'AttributeType': 'S'
        },
        {
                'AttributeName': 'email',
                'AttributeType': 'S'
        }
      ],
      'KeySchema': [
          {
              'AttributeName': 'pk',
              'KeyType': 'HASH'
          },
          {
              'AttributeName': 'sk',
              'KeyType': 'RANGE'
          },
      ],
      'GlobalSecondaryIndexes' : [
          {
              'IndexName': 'GSI_sk-pk',
              'KeySchema': [
                  {
                      'AttributeName': 'sk',
                      'KeyType': 'HASH'
                  },
                  {
                      'AttributeName': 'pk',
                      'KeyType': 'RANGE'
                  }
              ],
              'Projection': {
                  'ProjectionType': 'ALL'
              },
              'ProvisionedThroughput': {
                  'ReadCapacityUnits': 5,
                  'WriteCapacityUnits': 5
              }

          },
          {   
              'IndexName': 'GSI-pk-WorkoutStatusDate',
              'KeySchema': [
                  {
                      'AttributeName': 'pk',
                      'KeyType': 'HASH'
                  },
                  {
                      'AttributeName': 'WorkoutStatusDate',
                      'KeyType': 'RANGE'
                  }
              ],
              'Projection': {
                  'ProjectionType': 'ALL'
              },
              'ProvisionedThroughput': {
                  'ReadCapacityUnits': 5,
                  'WriteCapacityUnits': 5
              }
          },
	      {
              'IndexName': 'GSI_email-sk',
              'KeySchema': [
                  {
                      'AttributeName': 'email',
                      'KeyType': 'HASH'
                  },
                  {
                      'AttributeName': 'sk',
                      'KeyType': 'RANGE'
                  }
              ],
              'Projection': {
                  'ProjectionType': 'ALL'
              },
              'ProvisionedThroughput': {
                  'ReadCapacityUnits': 5,
                  'WriteCapacityUnits': 5
              }
          }
        ],
        'ProvisionedThroughput':{
          'ReadCapacityUnits': 5,
          'WriteCapacityUnits': 5
      }
    }
